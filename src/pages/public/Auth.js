import React from 'react'
import { Link } from 'react-router-dom'
import Login from '../../components/Login'
import Register from '../../components/Register'

const Auth = () => {
    return (
        <div>
            <Link to="/">Home</Link>
            <hr/>
            <Login/>
            <hr/>
            <Register/>
        </div>
    )
}

export default Auth;
