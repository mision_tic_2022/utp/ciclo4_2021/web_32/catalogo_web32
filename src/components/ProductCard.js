import React, { useContext, useState } from "react";
import { Card, Button } from "react-bootstrap";
import ProductContext from "../context/ProductContext";
import ShopContext from "../context/ShopContext";
import ProductFormModal from "./ProductFormModal";

const ProductCard = ({ objProduct, edit }) => {
  const { setProduct } = useContext(ProductContext);
  const {handleCart} = useContext(ShopContext);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleUpdate = (obj) => {
      setProduct(obj);
  };

  return (
    <>
      <Card style={{ width: "18rem" }}>
        <Card.Img height="220px" width="150px" variant="top" src={objProduct.url_img} />
        <Card.Body>
          <Card.Title>{objProduct.name}</Card.Title>
          <Card.Text>${objProduct.price}</Card.Text>
          {edit ? 
          (
            <>
            <Button variant="warning" onClick={handleShow}>
            Edit
          </Button>
          &nbsp;
          <Button variant="danger">Delete</Button>
            </>
          )
          :
          <Button variant="success" onClick={()=>handleCart(objProduct)}>Add cart</Button>
          }
          
        </Card.Body>
      </Card>
      {/********Ventana emergente (modal)*****/}
      <ProductFormModal
        show={show}
        handleClose={handleClose}
        objProduct={objProduct}
        handleUpdate={handleUpdate}
      />
    </>
  );
};

export default ProductCard;
