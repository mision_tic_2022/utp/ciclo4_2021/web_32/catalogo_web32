import React, { useContext, useState } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import AuthContext from "../context/AuthContext";

const objForm = {
    name: "",
    lastname: "",
    email: "",
    password: ""
}

const Register = () => {
    const {handleRegister} = useContext(AuthContext);
    const [form, setForm] = useState(objForm);

    const handleForm = (e)=>{
        let obj = {...form, [e.target.name]: e.target.value};
        setForm(obj);
    }

    const handleSubmit = (e)=>{
        e.preventDefault();
        handleRegister(form);
        setForm(objForm);
    }

  return (
    <div>
      <h2>Register</h2>
      <Form onSubmit={handleSubmit}>
        {/***********FILA 1********/}
        <Row>
          {/**Nombre**/}
          <Col>
            <Form.Group className="mb-3" controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control required name="name" value={form.name} onChange={handleForm} type="text" placeholder="Enter name" />
            </Form.Group>
          </Col>
          {/**Apellido**/}
          <Col>
            <Form.Group className="mb-3" controlId="lastname">
              <Form.Label>Lastname</Form.Label>
              <Form.Control required name="lastname" value={form.lastname} onChange={handleForm} type="text" placeholder="Enter lastname" />
            </Form.Group>
          </Col>
        </Row>
        {/***********FILA 2********/}
        <Row>
          {/**Email**/}
          <Col>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control required name="email" value={form.email} onChange={handleForm} type="email" placeholder="Enter email" />
            </Form.Group>
          </Col>
          {/**Password**/}
          <Col>
            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control required name="password" value={form.password} onChange={handleForm} type="password" placeholder="Password" />
            </Form.Group>
          </Col>
        </Row>

        <Button variant="primary" type="submit">
          Register
        </Button>
      </Form>
    </div>
  );
};

export default Register;
