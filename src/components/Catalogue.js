import React, { useContext, useState } from "react";
import ProductContext from "../context/ProductContext";
import ProductCard from "./ProductCard";
import "./Components.css";
import { Button } from "react-bootstrap";
import ShopContext from "../context/ShopContext";
import CartModal from "./CartModal";

const Catalogue = () => {
  const { catalogue } = useContext(ProductContext);
  const { cart } = useContext(ShopContext);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const viewCart = () => {
    setShow(true);
  };

  return (
    <div>
      <h2>Catalogue</h2>
      <Button onClick={viewCart}>Cart</Button>
      <div className="contCard">
        {catalogue.map((e) => (
          <ProductCard key={e._id} objProduct={e} edit={false} />
        ))}
      </div>
      {/******Modal****/}
      <CartModal show={show} handleClose={handleClose} cart={cart}/>
    </div>
  );
};

export default Catalogue;
