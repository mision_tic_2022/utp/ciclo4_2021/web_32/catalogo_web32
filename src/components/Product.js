import React, { useContext } from "react";
import ProductCard from "./ProductCard";
import ProductForm from "./ProductForm";
import ProductContext from "../context/ProductContext";
import "./Components.css"

const Product = () => {
  const { products } = useContext(ProductContext);

  return (
    <div>
      <h2>Product</h2>
      <ProductForm />
      <div className="contCard">
        {products.map((e) => (
          <ProductCard
            key={e._id}
            objProduct={e}
            edit={true}
          />
        ))}
      </div>
    </div>
  );
};

export default Product;
